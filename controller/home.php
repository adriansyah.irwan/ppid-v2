    function pengajuan_keberatan(){
		$data['menu'] = 'pengajuan_keberatan';
		$data['menu2'] = 'pengajuan-keberatan';
		$data['menu3'] = '';

		if(empty($_POST)){
			$data = array_merge($data, $this->data);
			$this->load->view('homeNew/pengajuan_keberatan', $data);
		}else{
			if($this->input->post("nik") == "" || $this->input->post("kode") == ""){
				$this->session->set_flashdata('gagal', 'Kode permohonan dan NIK harus diisi');
				redirect('pengajuan-keberatan');
				return true;
			}else{

				$nik = $this->input->post("nik");
				$kode_permohonan = $this->input->post("kode");

				/* CHECK status kode permohonan */
				$params = new stdClass;
				$params->select = "*";
				$params->table = "object_permohonan";
				$params->wherefull = "kode_permohonan='".$kode_permohonan."' AND nik_no_ktp='".$nik."' AND deleted = false AND status_permohonan = 2";
				$cekPermohonan = $this->custom_crud->viewSingle($params);
				/* CHECK status kode permohonan */

				if(!$cekPermohonan->status){
					$this->session->set_flashdata('gagal', 'Pengajuan keberatan untuk kode permohonan '.$kode_permohonan.' tidak dapat dilakukan. Silahkan cek kembali status permohonan anda <a href="'.base_url('cek-status-permohonan-informasi').'">klik disini</a>');
					redirect('pengajuan-keberatan');
					return true;
				}else{
					/* CHECK status kode pengajuan keberatan */
					$params = new stdClass; //ini object
					$params->select = "*";
					$params->table = "object_keberatan";
					$params->wherefull = "kode_permohonan='".$kode_permohonan."' AND aktif = 1";
					$cekKeberatan = $this->custom_crud->viewSingle($params);
					/* CHECK status kode pengajuan keberatan */

					if($cekKeberatan->status){
						/* IF Exist maka berikan warning */						
						$this->session->set_flashdata('gagal', 'Pengajuan keberatan untuk kode permohonan '.$kode_permohonan.' sudah mengajukan keberatan. Silahkan cek kembali status pengajuan keberatan anda <a href="'.base_url('cek-status-keberatan').'">klik disini</a>');
						redirect('pengajuan-keberatan');
						return true;
					}else{
						/* Jika belum mengajukan keberatan maka lanjutkan tampil form */
						$data['data_permohonan'] = $cekPermohonan->value;
						$data = array_merge($data, $this->data);
						$this->load->view('homeNew/pengajuan_keberatan_form', $data);
					}
				}
			}
		}
		// $this->error_page();
		
		// $data['menu'] = 'layanan_informasi';
		// $data['menu2'] = 'permohonan-keberatan';
		// $data['menu3'] = '';
		
		// if($this->input->post("no") != "" || $this->input->post("email") != ""){
		// 	$no = $this->input->post("no");
		// 	$email = $this->input->post("email");
		// 	/* $where = "(permohonan.kode_permohonan='".$no."' AND user.email='".$email."' AND permohonan.status_permohonan = '2')"; */
		// 	$where = "(permohonan.kode_permohonan='".$no."' AND user.email='".$email."')";
		// 	$cek = $this->simple_crud->cek_status_permohonan($where, 1);
			
		// 	if($cek->num_rows() == '1'){
				
		// 		if(($cek->row()->batas_waktu_tambahan != NULL && strtotime($cek->row()->batas_waktu_tambahan) < strtotime(date('Y-m-d H:i:s'))) || $cek->row()->status_permohonan == '2'){
		// 			$cek2 = $this->simple_crud->view_by("keberatan", "kode_permohonan", $cek->row()->kode_permohonan);
		// 			if($cek2->num_rows() == '0'){
		// 				$data['data'] = $cek;
		// 				$this->load->view('homeNew/permohonan_keberatan_form', $data);
		// 			}else{
		// 				$this->session->set_flashdata('gagal', 'Nomor Pendaftaran : <b>'.$no.'</b><br />Catatan: <b>No Pendaftaran tersebut sudah mengajukan keberatan.</b><br /> Klik <a href="'.base_url('cek-status-permohonan-keberatan').'">Cek Status Permohonan Keberatan</a> untuk cek status permohonan keberatan Anda.');
		// 				redirect('permohonan-keberatan');
		// 			}
		// 		}
		// 		elseif(strtotime($cek->row()->batas_waktu) < strtotime(date('Y-m-d H:i:s')) || $cek->row()->status_permohonan == '2'){
		// 			$cek2 = $this->simple_crud->view_by("keberatan", "kode_permohonan", $cek->row()->kode_permohonan);
		// 			if($cek2->num_rows() == '0'){
		// 				$data['data'] = $cek;
		// 				$this->load->view('homeNew/permohonan_keberatan_form', $data);
		// 			}else{
		// 				$this->session->set_flashdata('gagal', 'Nomor Pendaftaran : <b>'.$no.'</b><br />Catatan: <b>No Pendaftaran tersebut sudah mengajukan keberatan.</b><br /> Klik <a href="'.base_url('cek-status-permohonan-keberatan').'">Cek Status Permohonan Keberatan</a> untuk cek status permohonan keberatan Anda.');
		// 				redirect('permohonan-keberatan');
		// 			}
		// 		}else{
		// 			$this->session->set_flashdata('info', 'Nomor Pendaftaran : <b>'.$no.'</b><br />Status Permohonan : <b>Masih dalam proses dan belum bisa mengajukan permohonan keberatan</b>');
		// 			redirect('permohonan-keberatan');
		// 		}
				
		// 	}else{
		// 		$this->session->set_flashdata('gagal', 'Nomor Pendaftaran : <b>'.$no.'</b><br />Catatan: <b>No Pendaftaran atau email tidak sesuai.</b>');
		// 		redirect('permohonan-keberatan');
		// 	}
		// }else{
		// 	$this->load->view('homeNew/permohonan_keberatan', $data);
		// }
	}
	function pengajuan_keberatanform($kode_permohonan,$nik){

		if(empty($kode_permohonan)||$kode_permohonan==''||empty($nik)||$nik==''){
			$this->session->set_flashdata('gagal', 'Kode permohonan dan NIK tidak valid');
			redirect('pengajuan-keberatan');
			return true;
		}

		/* CHECK status kode permohonan */
		$params = new stdClass;
		$params->select = "*";
		$params->table = "object_permohonan";
		$params->wherefull = "kode_permohonan='".$kode_permohonan."' AND nik_no_ktp='".$nik."' AND deleted = false AND status_permohonan = 2 AND aktif = 1";
		$cekPermohonan = $this->custom_crud->viewSingle($params);
		/* CHECK status kode permohonan */

		if(!$cekPermohonan->status){
			$this->session->set_flashdata('gagal', 'Pengajuan keberatan untuk kode permohonan '.$kode_permohonan.' tidak dapat dilakukan. Silahkan cek kembali status permohonan anda <a href="'.base_url('cek-status-permohonan-informasi').'">klik disini</a>');
			redirect('pengajuan-keberatan');
			return false;
		}else{
			/* CHECK status kode pengajuan keberatan */
			$params = new stdClass; //ini object
			$params->select = "*";
			$params->table = "object_keberatan";
			$params->wherefull = "kode_permohonan='".$kode_permohonan."'";
			$cekKeberatan = $this->custom_crud->viewSingle($params);
			/* CHECK status kode pengajuan keberatan */

			if($cekKeberatan->status){
				/* IF Exist maka berikan warning */						
				$this->session->set_flashdata('gagal', 'Pengajuan keberatan untuk kode permohonan '.$kode_permohonan.' sudah mengajukan keberatan. Silahkan cek kembali status pengajuan keberatan anda <a href="'.base_url('cek-status-keberatan').'">klik disini</a>');
				redirect('pengajuan-keberatan');
				return false;
			}
		}
		if(empty($_POST)){
			/* Jika belum mengajukan keberatan maka lanjutkan tampil form */
			$data['data_permohonan'] = $cekPermohonan->value;
			$data = array_merge($data, $this->data);
			$this->load->view('homeNew/pengajuan_keberatan_form', $data);
			return true;
		}else{
			/* jika terdapat method post maka simpan keberatan */
			if(!$this->keberatan($cekPermohonan->value)){
				/* Jika belum mengajukan keberatan maka lanjutkan tampil form */
				$data['data_permohonan'] = $cekPermohonan->value;
				$data = array_merge($data, $this->data);
				$this->load->view('homeNew/pengajuan_keberatan_form', $data);
				return true;
			}
		}
	}
	
	function keberatan($dataPermohonan){

		$kode_permohonan = $dataPermohonan->kode_permohonan;
		$email = $dataPermohonan->email; //email pemohon
		$created = date('YmdHis');
		$nama_kuasa = $this->input->post("nama_kuasa");
		$alamat_kuasa = $this->input->post("addr_kuasa");
		$no_telp_kuasa = $this->input->post("tlp_kuasa");
		
		$dataPost = array();
		
		$dataPost['nama_kuasa'] = $nama_kuasa;
		$dataPost['alamat_kuasa'] = $alamat_kuasa;
		$dataPost['no_telp_kuasa'] = $no_telp_kuasa;
		// $dataPost['kasus_posisi'] = $kasus_posisi;
		$dataPost['kode_permohonan'] = $kode_permohonan;
		
		$alasan_a = array();
		$alasan_exist = false;
		if(array_key_exists("alasan_keberatan_a",$_POST)){
			$alasan_exist = true;
			$alasan_a = array(
					array(
							'field'   => 'alasan_keberatan_a',
							'label'   => 'Alasan Keberatan',
							'rules'   => 'required|min_length[2]|max_length[2]'
					),
			);
			$dataPost['alasan_keberatan_a'] = $this->input->post('alasan_keberatan_a');
		}
		
		$alasan_b = array();
		if(array_key_exists("alasan_keberatan_b",$_POST)){
			$alasan_exist = true;
			$alasan_b = array(
					array(
							'field'   => 'alasan_keberatan_b',
							'label'   => 'Alasan Keberatan',
							'rules'   => 'required|min_length[2]|max_length[2]'
					),
			);
			$dataPost['alasan_keberatan_b'] = $this->input->post('alasan_keberatan_b');
		}
		
		$alasan_c = array();
		if(array_key_exists("alasan_keberatan_c",$_POST)){
			$alasan_exist = true;
			$alasan_c = array(
					array(
							'field'   => 'alasan_keberatan_c',
							'label'   => 'Alasan Keberatan',
							'rules'   => 'required|min_length[2]|max_length[2]'
					),
			);
			$dataPost['alasan_keberatan_c'] = $this->input->post('alasan_keberatan_c');
		}
		
		$alasan_d = array();
		if(array_key_exists("alasan_keberatan_d",$_POST)){
			$alasan_exist = true;
			$alasan_d = array(
					array(
							'field'   => 'alasan_keberatan_d',
							'label'   => 'Alasan Keberatan',
							'rules'   => 'required|min_length[2]|max_length[2]'
					),
			);
			$dataPost['alasan_keberatan_d'] = $this->input->post('alasan_keberatan_d');
		}
		
		$alasan_e = array();
		if(array_key_exists("alasan_keberatan_e",$_POST)){
			$alasan_exist = true;
			$alasan_e = array(
					array(
							'field'   => 'alasan_keberatan_e',
							'label'   => 'Alasan Keberatan',
							'rules'   => 'required|min_length[2]|max_length[2]'
					),
			);
			$dataPost['alasan_keberatan_e'] = $this->input->post('alasan_keberatan_e');
		}
		
		$alasan_f = array();
		if(array_key_exists("alasan_keberatan_f",$_POST)){
			$alasan_exist = true;
			$alasan_f = array(
					array(
							'field'   => 'alasan_keberatan_f',
							'label'   => 'Alasan Keberatan',
							'rules'   => 'required|min_length[2]|max_length[2]'
					),
			);
			$dataPost['alasan_keberatan_f'] = $this->input->post('alasan_keberatan_f');
		}
		
		$alasan_g = array();
		if(array_key_exists("alasan_keberatan_g",$_POST)){
			$alasan_exist = true;
			$alasan_g = array(
					array(
							'field'   => 'alasan_keberatan_g',
							'label'   => 'Alasan Keberatan',
							'rules'   => 'required|min_length[2]|max_length[2]'
					),
			);
			$dataPost['alasan_keberatan_g'] = $this->input->post('alasan_keberatan_g');
		}
		
		
		$kuasa = array();
		if($nama_kuasa != "" || $alamat_kuasa != "" || $no_telp_kuasa != ""){
			$kuasa = array(
						array(
								'field'   => 'nama_kuasa',
								'label'   => 'Nama Lengkap Kuasa',
								'rules'   => 'min_length[5]|max_length[30]'
						),
						array(
								'field'   => 'alamat_kuasa',
								'label'   => 'Alamat Kuasa',
								'rules'   => 'min_length[20]'
						),
						array(
								'field'   => 'no_kuasa',
								'label'   => 'No Telepon Kuasa',
								'rules'   => 'numeric|min_length[12]'
						),
					);
		}

		$configAlasanNotExist = array();
		if(!$alasan_exist){
			$configAlasanNotExist = array(
				array(
						'field'   => 'alasan_exist',
						'label'   => 'Alasan Keberatan',
						'rules'   => 'required'
				),
		
			);
		}

		$_POST["file"] = $_FILES["file"]["size"];
		$ext_exp = explode('.', $_FILES["file"]["name"]);
		$ext = $ext_exp[count($ext_exp)-1];
		$configUpload = array(
			array(
					'field'   => 'file',
					'label'   => 'Surat Keberatan',
					'rules'   => 'required|callback_checkUploadKeberatan['.$kode_permohonan.']'
			),
	
		);
		
		$config = array_merge($kuasa, $alasan_a, $alasan_b, $alasan_c, $alasan_d, $alasan_e, $alasan_f, $alasan_g, $configAlasanNotExist, $configUpload);
	
		$this->form_validation->set_error_delimiters('<div class="has-error"><label for="inputError" class="control-label">', '</label></div>');
		$this->form_validation->set_message('required', 'Kolom %s harus diisi.');
		$this->form_validation->set_message('valid_email', 'Email tidak valid.');
		$this->form_validation->set_message('numeric', 'Hanya boleh angka.');
		$this->form_validation->set_message('check_default', 'Hak akses harus di pilih.');
		$this->form_validation->set_message('min_length', 'Minimal karakter %s ialah %s karakter.');
		$this->form_validation->set_message('max_length', 'Maksimal karakter %s ialah %s karakter.');
		$this->form_validation->set_message('cek_secure', 'Hanya boleh huruf atau angka.');
		$this->form_validation->set_rules($config);
	
		if ($this->form_validation->run() == FALSE)
		{
			return false;
		}
		else
		{
			// $this->load->library('encrypt');
			$this->load->library('encryption');
			
			$tgl_buat = date('Y-m-d H:i:s');
			$dataPost['tgl_buat_keberatan'] = $tgl_buat;
			
			$week6 = date("Y-m-d 23:59:59", strtotime($tgl_buat. '+6 weeks'));
			
			$day = date("w", strtotime($week6));
			
			$hari_kerja = $week6;
			
			if($day == 0){
				$hari_kerja = date("Y-m-d 23:59:59", strtotime($week6. '+1 day'));
			}
			if($day == 6){
				$hari_kerja = date("Y-m-d 23:59:59", strtotime($week6. '+2 day'));
			}
			
			$dataPost['batas_waktu_keberatan'] = $hari_kerja;
			
			$tgl = date("YmdHis").'keberatan';
			// $id = $this->encrypt->encode($tgl);
			$id = $this->encryption->encrypt($tgl);
			$id_keberatan_1st = substr(clean($id), 0, 15);
			
			$cek_id_keberatan = $this->simple_crud->view_by('keberatan','id_keberatan',$id_keberatan_1st);
			$id_keberatan_2nd = $cek_id_keberatan->num_rows();
			
			while($id_keberatan_2nd > 0){
				$tgl = date("YmdHis").'keberatan';
				// $id = $this->encrypt->encode($tgl);
				$id = $this->encryption->encrypt($tgl);
				$id_keberatan_1st = substr(clean($id), 0, 15);
			
				$cek_id_keberatan = $this->simple_crud->view_by('keberatan','id_keberatan',$id_keberatan_1st);
				$id_keberatan_2nd = $cek_id_keberatan->num_rows();
			}
			
			$id_keberatan = $id_keberatan_1st;
			
			$created_date = date("Y-m-d H:i:s");

			$get_last = $this->simple_crud->view_custom_by('keberatan', "id_keberatan != '' AND no_reg_keberatan LIKE '".date("Y")."0%-KBI'", "no_reg_keberatan", "desc", 1);
			
			$kode_keberatan = "";

			if($get_last->num_rows() > 0){
				$kode_keberatan = $get_last->row()->no_reg_keberatan;
			}
				
			if($kode_keberatan==''){
				$no_reg_keberatan = date('Y').'00001-KBI';
			}else{
				$exp = explode('-', $kode_keberatan);
				$no_reg_keberatan = ($exp[0]+1).'-'.(string)$exp[1];
			}
			
			$path_keberatan = str_replace(".","", $kode_permohonan.'/'.strtolower(preg_replace("/([^a-zA-Z0-9\.]+)/","-","file-keberatan")));
			//table Keberatan
			$arrInp = array(
					"id_keberatan" => $id_keberatan,
					"no_reg_keberatan" => $no_reg_keberatan,
					"created_date" => date("Y-m-d H:i:s"),
					"status_keberatan" => 1,
					"status_email_keberatan" => true,
					"cara_keberatan" => "online",
					"path_file_keberatan" => $path_keberatan.'.'.$ext,
			);
				
			$dataIns = array_merge($dataPost, $arrInp);

			$this->simple_crud->insert("keberatan", $dataIns);

			$this->send_email_keberatan($id_keberatan, $kode_permohonan, $email);
			
			$this->session->set_flashdata('sukses', 'Terima kasih, permohonan keberatan Anda akan kami proses. Untuk nomor registrasi keberatan dan tanda terima permohonan keberatan akan dikirimkan melalui email, silahkan lihat pada menu kotak masuk (inbox) atau spam pada email Anda, mohon tunggu dalam 1x24 jam pada hari dan jam kerja.');
			redirect('pengajuan-keberatan');
			
		}
	}

	function checkUploadKeberatan($id,$kode_permohonan){

		/* UPLOAD File Keberatan */
		if($_FILES['file']['name'] != ''){
			$created = date('YmdHis');
			$path_keberatan = './UPLOAD/Data_Keberatan/'.$kode_permohonan;
			if(!file_exists($path_keberatan)) {
            	mkdir($path_keberatan, 0777, TRUE);
            }
			$config['upload_path'] = $path_keberatan;
			$config['allowed_types'] = '*';
			$config['allowed_types'] = 'gif|jpg|jpeg|png|pdf|bmp|zip|rar';
			$config['max_size'] = '5120';
			$config['overwrite'] = TRUE;
			$filename = str_replace(".","", strtolower(preg_replace("/([^a-zA-Z0-9\.]+)/","-","file-keberatan")));
	
			$config['file_name'] = $filename;
	
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
	
			if ( ! $this->upload->do_upload("file"))
			{
				$this->form_validation->set_message('checkUploadKeberatan', 'Error upload surat keberatan ['.$this->upload->display_errors().']');
				return false;
			}
			else{
				$file = array('upload_data' => $this->upload->data());
				$path = $file['upload_data']['file_name'];
			}
		}
		/* UPLOAD File Keberatan */
		return true;
	}